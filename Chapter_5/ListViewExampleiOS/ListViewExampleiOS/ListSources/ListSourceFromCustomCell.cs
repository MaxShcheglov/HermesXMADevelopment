﻿using System;
using System.Collections.Generic;
using Foundation;
using ListViewExampleiOS.Models;
using UIKit;

namespace ListViewExampleiOS.ListSources
{
	public class ListSourceFromCustomCell : UITableViewSource
	{
		protected List<ListItem> listItems;
		protected string CellId = "TableCell";

		public ListSourceFromCustomCell(List<ListItem> items)
		{
			listItems = items;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(CellId) as CustomCell;
			if (cell == null)
				cell = new CustomCell((NSString)CellId);
			cell.UpdateCell(listItems[indexPath.Row].Title, listItems[indexPath.Row].Description);

			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return listItems.Count;
		}
	}
}
