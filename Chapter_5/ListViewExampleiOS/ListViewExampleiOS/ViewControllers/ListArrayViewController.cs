﻿using UIKit;
using ListViewExampleiOS.ListSources;

namespace ListViewExampleiOS.ViewControllers
{
	public class ListArrayViewController : UIViewController
	{
		public ListArrayViewController()
		{
		}

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// Perform any additional setup after loading the view

			UITableView table = new UITableView(View.Bounds);
			string[] tableItems = new string[] {"First", "Second", "Third", "Fourth", "Fifth"};
			table.Source = new ListSourceFromArray(tableItems);
			Add(table);
		}
	}
}