﻿using Foundation;
using ListViewExampleiOS.ListSources;
using ListViewExampleiOS.Models;
using System.Collections.Generic;
using UIKit;

namespace ListViewExampleiOS.ViewControllers
{
	class ListModelViewControllerCellReuse : UIViewController
	{
		List<ListItem> listItems;
		public static NSString CellId = new NSString("CellId");

		public ListModelViewControllerCellReuse()
		{
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			UITableView table = new UITableView(View.Bounds);
			listItems = new List<ListItem> {
				new ListItem {Title = "First", Description="1st item"},
				new ListItem {Title = "Second", Description="2nd item"},
				new ListItem {Title = "Third", Description="3rd item"}
			};
			table.RegisterClassForCellReuse(typeof(CustomCell), CellId);
			table.Source = new ListSourceFromModelCellReuse(listItems);
			Add(table);
		}
	}
}