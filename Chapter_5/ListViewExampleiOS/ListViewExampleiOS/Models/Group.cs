﻿using System.Collections.Generic;

namespace ListViewExampleiOS.Models
{
	public class Group : List<ListItem>
	{
		public string Key { get; private set; }

		public Group(string key, List<ListItem> items)
		{
			Key = key;
			foreach (var item in items)
				this.Add(item);
		}
	}
}