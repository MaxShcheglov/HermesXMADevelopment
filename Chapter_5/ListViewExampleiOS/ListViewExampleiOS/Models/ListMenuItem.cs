﻿using System;

namespace ListViewExampleiOS.Models
{
	public class ListMenuItem
	{
		public string Title { get; set; }
		public Type PageType { get; set; }
	}
}