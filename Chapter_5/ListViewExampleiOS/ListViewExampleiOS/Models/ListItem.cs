﻿namespace ListViewExampleiOS.Models
{
	public class ListItem
	{
		public string Title { get; set; }
		public string Description { get; set; }
	}
}