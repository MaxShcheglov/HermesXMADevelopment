﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace ListViewExample
{
	public class ListViewStrings : ContentPage
	{
		public ListViewStrings()
		{
			var items = new List<string>() { "First", "Second", "Third" };
			var listView = new ListView();
			listView.ItemsSource = items;

			listView.ItemTapped += async (sender, e) =>
			{
				await DisplayAlert("Tapped", e.Item.ToString() + " was selected.", "OK");
				((ListView)sender).SelectedItem = null;
			};

			this.Padding = new Thickness(0, Device.OnPlatform(20, 0, 0), 0, 0);

			this.Content = listView;
		}
	}
}