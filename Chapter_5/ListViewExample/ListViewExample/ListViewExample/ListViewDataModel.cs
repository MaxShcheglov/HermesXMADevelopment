﻿using Xamarin.Forms;

namespace ListViewExample
{
	public class ListViewDataModel : ContentPage
    {
		public ListViewDataModel()
		{
			var listView = new ListView();
			listView.ItemsSource = new ListItem[]
			{
				new ListItem { Title="First", Description = "1st item" },
				new ListItem { Title="Second", Description = "2nd item" },
				new ListItem { Title="Third", Description = "3rd item" }
			};

			var template = new DataTemplate(typeof(TextCell));
			template.SetValue(TextCell.TextColorProperty, Color.Red);
			template.SetValue(TextCell.DetailColorProperty, Color.Blue);

			listView.ItemTemplate = template;
			listView.ItemTemplate.SetBinding(TextCell.TextProperty, "Title");
			listView.ItemTemplate.SetBinding(TextCell.DetailProperty, "Description");

			listView.ItemTapped += async (sender, e) =>
			{
				ListItem item = (ListItem)e.Item;
				await DisplayAlert("Tapped", item.Title + " was selected.", "OK");
				((ListView)sender).SelectedItem = null;
			};

			this.Padding = new Thickness(0, Device.OnPlatform(20, 0, 0), 0, 0);

			this.Content = listView;
		}

		public class ListItem
		{
			public string Title { get; set; }
			public string Description { get; set; }
		}
	}
}
