﻿using System;
using Xamarin.Forms;

namespace ListViewExample
{
	public class HomePage : ContentPage
    {
		public HomePage()
		{
			Title = "Xamarin.Forms ListView - Chapter 5";

			var listView = new ListView();
			listView.ItemsSource = new ListItemPage[]
			{
				new ListItemPage { Title = "Bind to a List of strings", PageType = typeof(ListViewStrings) },
				new ListItemPage { Title = "Bind to a Data Model", PageType = typeof(ListViewDataModel) },
				new ListItemPage { Title = "Bind to a Image Cell", PageType = typeof(ListViewImageCell) },
				new ListItemPage { Title = "Bind to a Custom Cell", PageType = typeof(ListViewCustom) },
				new ListItemPage { Title = "Bind to a Custom Cell with Button", PageType = typeof(ListViewButton) },
				new ListItemPage { Title = "Bind to a Custom Cell with ContextActions", PageType = typeof(ListViewContextAction) },
				new ListItemPage { Title = "Bind with Grouping", PageType = typeof(ListViewGrouped)},
				new ListItemPage { Title = "Bind with Custom Grouping Template", PageType = typeof(ListViewGroupingTemplate) },
				new ListItemPage { Title = "Bind with Scroll View", PageType = typeof(ListViewScroll) },
				new ListItemPage { Title = "Bind with Performance", PageType = typeof(ListViewPerformance) }
			};

			listView.ItemTemplate = new DataTemplate(typeof(TextCell));
			listView.ItemTemplate.SetBinding(TextCell.TextProperty, "Title");

			listView.ItemTapped += async (sender, args) =>
			{
				var item = args.Item as ListItemPage;
				if (item == null)
					return;

				Page page = (Page)Activator.CreateInstance(item.PageType);
				await Navigation.PushAsync(page);
				listView.SelectedItem = null;
			};

			Content = listView;
		}
    }

	public class ListItemPage
	{
		public string Title { get; set; }
		public Type PageType { get; set; }
	}
}
