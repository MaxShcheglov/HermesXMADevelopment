﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Views;
using Android.Widget;

namespace ListViewExampleAndroid
{
	public class ListGroupAdapter : BaseAdapter
	{
		private List<ListItem> itemList;
		private Activity context;

		public ListGroupAdapter(Activity context, List<ListItem> items) : base()
		{
			this.context = context;
			this.itemList = items;
		}

		public override int Count => itemList.Count;

		public override Java.Lang.Object GetItem(int position)
		{
			throw new NotImplementedException();
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{

			View view;

			if (itemList[position].IsGroupHeader)
			{
				view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
				view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = itemList[position].Title;
			}
			else
			{
				view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem2, null);
				view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = itemList[position].Title;
				view.FindViewById<TextView>(Android.Resource.Id.Text2).Text = itemList[position].Description;
			}

			return view;
		}
	}
}