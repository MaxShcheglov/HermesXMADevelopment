﻿namespace ListViewExampleAndroid
{
	public class ListItem
	{
		public string Title { get; set; }
		public string Description { get; set; }
		public bool IsGroupHeader { get; set; }
	}
}