﻿using Android.App;
using Android.OS;
using Android.Widget;
using System.Collections.Generic;

namespace ListViewExampleAndroid
{
	[Activity(Label = "MainCustomListActivity")]
	public class MainCustomListActivity : Activity
	{
		List<ListItem> listItems;
		ListView listView;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			listItems = new List<ListItem>
			{
				new ListItem { Title = "First", Description = "1st item" },
				new ListItem { Title = "Second", Description = "2nd item" },
				new ListItem { Title = "Third", Description = "3rd item" },
			};

			SetContentView(Resource.Layout.HomeLayout);
			listView = FindViewById<ListView>(Resource.Id.listItems);
			listView.Adapter = new ListCustomAdapter(this, listItems);
			listView.ItemClick += OnListItemClick;
		}

		private void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			string selectedItem = listItems[e.Position].Title;
			Toast.MakeText(this, selectedItem, ToastLength.Short).Show();
		}
	}
}