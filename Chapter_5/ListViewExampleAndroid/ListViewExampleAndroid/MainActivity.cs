﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Views;

namespace ListViewExampleAndroid
{
	[Activity(Label = "ListViewExampleAndroid", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : ListActivity
	{
		List<MenuListItem> listItems;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			listItems = new List<MenuListItem>
			{
				new MenuListItem {Title = "Bind to Array Of Strings", PageType = typeof(ListActivityArray)},
				new MenuListItem {Title = "Bind to Data Model", PageType = typeof(ListActivityDataModel)},
				new MenuListItem {Title = "Customize List", PageType= typeof(MainCustomListActivity)},
				new MenuListItem {Title = "Grouping a list", PageType= typeof(MainActivityGrouped)},
			};

			ListAdapter = new MenuListItemAdapter(this, listItems);
		}

		protected override void OnListItemClick(ListView l, View v, int position, long id)
		{
			StartActivity(listItems[position].PageType);
		}
	}
}

