﻿using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace ListViewExampleAndroid
{
	public class MenuListItemAdapter : BaseAdapter
	{
		private Activity context;
		private List<MenuListItem> itemList;

		public MenuListItemAdapter(Activity context, List<MenuListItem> itemList)
		{
			this.context = context;
			this.itemList = itemList;
		}

		public override int Count => itemList.Count;

		public override Object GetItem(int position)
		{
			throw new System.NotImplementedException();
		}

		public override long GetItemId(int position) => position;

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;
			if (view == null)
				view = context.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
			view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = itemList[position].Title;

			return view;
		}
	}
}