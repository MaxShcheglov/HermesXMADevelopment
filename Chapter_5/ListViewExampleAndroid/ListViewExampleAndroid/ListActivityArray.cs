﻿
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ListViewExampleAndroid
{
	[Activity(Label = "ListActivityArray")]
	public class ListActivityArray : ListActivity
	{
		string[] listItems;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			listItems = new string[] { "First", "Second", "Third" };
			ListAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, listItems);
		}

		protected override void OnListItemClick(ListView l, View v, int position, long id)
		{
			string selectedItem = listItems[position];
			Toast.MakeText(this, selectedItem, ToastLength.Short).Show();
		}
	}
}