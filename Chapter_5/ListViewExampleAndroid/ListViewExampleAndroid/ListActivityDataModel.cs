﻿using Android.App;
using Android.OS;
using System.Collections.Generic;

namespace ListViewExampleAndroid
{
	[Activity(Label = "ListActivityDataModel")]
	public class ListActivityDataModel : ListActivity
	{
		List<ListItem> listItems;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			List<ListItem> listItems = new List<ListItem>
			{
				new ListItem { Title = "First", Description = "1st item" },
				new ListItem { Title = "Second", Description = "2nd item" },
				new ListItem { Title = "Third", Description = "3rd item" }, 
			};

			ListAdapter = new ListItemAdapter(this, listItems);
		}
	}
}