﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DataBindingEx
{
	public class ItemBindable : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		string _title;
		string _description;

		public ItemBindable()
		{
		}

		public string Title
		{
			get => _title;
			set
			{
				if (value.Equals(_title, StringComparison.Ordinal))
					return;

				_title = value;
				OnPropertyChanged("Title");
			}
		}

		public string Description
		{
			get => _description;
			set
			{
				if (value.Equals(_description, StringComparison.Ordinal))
					return;

				_description = value;
				OnPropertyChanged("Description");
			}
		}

		void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}