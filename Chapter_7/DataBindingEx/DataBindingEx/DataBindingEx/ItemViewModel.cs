﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DataBindingEx
{
	public class ItemViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		Item _item;

		public ItemViewModel()
		{
			_item = new Item();
		}

		public string Title
		{
			get => _item.Title;
			set
			{
				if (value.Equals(_item.Title, StringComparison.Ordinal))
					return;

				_item.Title = value;
				OnPropertyChanged("Title");
			}
		}

		void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
