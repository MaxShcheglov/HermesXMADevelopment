﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DataBindingEx
{
	public class ObservableItem : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		Item _item;

		public ObservableItem()
		{
			_item = new Item();
		}

		public string Title
		{
			get => _item.Title;
			set
			{
				if (value.Equals(_item.Title, StringComparison.Ordinal))
					return;

				_item.Title = value;
				OnPropertyChanged("Title");
			}
		}

		public string Description
		{
			get => _item.Description;
			set
			{
				if (value.Equals(_item.Description, StringComparison.Ordinal))
					return;

				_item.Description = value;
				OnPropertyChanged("Description");
			}
		}

		void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}