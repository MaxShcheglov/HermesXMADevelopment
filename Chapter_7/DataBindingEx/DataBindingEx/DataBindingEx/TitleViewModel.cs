﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace DataBindingEx
{
	public class TitleViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		String _title;

		public string Title
		{
			get => _title;
			set
			{
				if (value.Equals(_title, StringComparison.Ordinal))
					return;

				_title = value;
				OnPropertyChanged("Title");

			}
		}

		void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
