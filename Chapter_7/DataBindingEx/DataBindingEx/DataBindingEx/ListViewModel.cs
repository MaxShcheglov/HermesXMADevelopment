﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DataBindingEx
{
    public class ListViewModel
    {
		ObservableCollection<ObservableItem> _items;

		public ListViewModel()
		{
			_items = new ObservableCollection<ObservableItem>
			{
				new ObservableItem {Title = "First", Description="1st item"},
				new ObservableItem {Title = "Second", Description="2nd item"},
				new ObservableItem {Title = "Third", Description="3rd item"}
			};
		}

		public ObservableCollection<ObservableItem> Items
		{
			get => _items;
			set
			{
				if (value == _items)
					return;

				_items = value;
			}
		}

		public void Replace()
		{
			Items = new ObservableCollection<ObservableItem>
			{
				new ObservableItem { Title = "Primero", Description = "First" },
				new ObservableItem { Title = "Segundo", Description = "Second" },
				new ObservableItem { Title = "Tercero", Description = "Third" },
			};

		}
    }
}
