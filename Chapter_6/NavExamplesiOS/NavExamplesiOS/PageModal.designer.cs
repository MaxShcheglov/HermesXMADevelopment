﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace NavigationExamplesiOS
{
    [Register ("PageModal")]
    partial class PageModal
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        NavigationExamplesiOS.PageModalView PageModalView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (PageModalView != null) {
                PageModalView.Dispose ();
                PageModalView = null;
            }
        }
    }
}