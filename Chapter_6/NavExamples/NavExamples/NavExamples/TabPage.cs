﻿using Xamarin.Forms;

namespace NavExamples
{
	public class TabPage : TabbedPage
    {
		public TabPage()
		{
			Title = "Tabbed Page";
			Children.Add(new HomePage()
			{
				Title = "Home Page"
			});
			Children.Add(new SecondPage());
			Children.Add(new ThirdPage());
		}
    }
}
