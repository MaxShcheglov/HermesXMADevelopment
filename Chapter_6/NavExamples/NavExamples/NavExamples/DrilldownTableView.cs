﻿using System;
using Xamarin.Forms;

namespace NavExamples
{
	public class DrilldownTableView : ContentPage
    {
		public DrilldownTableView()
		{
			Command<Type> navigateCommand = new Command<Type>(async (Type pageType) =>
			{
				Page page = (Page)Activator.CreateInstance(pageType);
				await this.Navigation.PushAsync(page);
			});

			Title = "Drilldown List Using TableView";
			this.Content = new TableView
			{
				Intent = TableIntent.Menu,
				Root = new TableRoot
					{
						new TableSection("Hindi")
						{
							new TextCell
							{
								Text = "Prathama",
								Command = navigateCommand,
								CommandParameter = typeof(FirstPage)
							},

							new TextCell
							{
								Text = "Dūsarā",
								Command = navigateCommand,
								CommandParameter = typeof(SecondPage)
							},

							new TextCell
							{
								Text = "Tīsarā",
								Command = navigateCommand,
								CommandParameter = typeof(ThirdPage)
							}
						},

						new TableSection("Español")
						{
							new TextCell
							{
								Text = "Primero",
								Command = navigateCommand,
								CommandParameter = typeof(FirstPage)
							},

							new TextCell
							{
								Text = "Segundo",
								Command = navigateCommand,
								CommandParameter = typeof(SecondPage)
							},

							new TextCell
							{
								Text = "Tercera",
								Command = navigateCommand,
								CommandParameter = typeof(ThirdPage)
							}
						},

						new TableSection("English")
						{
							new TextCell
							{
								Text = "First",
								Command = navigateCommand,
								CommandParameter = typeof(FirstPage)
							},

							new TextCell
							{
								Text = "Second",
								Command = navigateCommand,
								CommandParameter = typeof(SecondPage)
							},

							new TextCell
							{
								Text = "Third",
								Command = navigateCommand,
								CommandParameter = typeof(ThirdPage)
							}
						}

					}
			};
		}
    }
}
