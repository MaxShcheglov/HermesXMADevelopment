﻿
using Xamarin.Forms;

namespace NavExamples
{
	public class NavigationPage1 : ContentPage
	{
		Button homeButton;

		public NavigationPage1 ()
		{
			Title = "Hierarchical Navigation";

			Label homeLabel = new Label
			{
				Text = "Home Page",
				FontSize = 40
			};

			homeButton = new Button
			{
				Text = "Go to Second Page"
			};

			homeButton.Clicked += async (sender, args) =>
				await Navigation.PushAsync(new NavigationPage2());

			StackLayout stackLayout = new StackLayout
			{
				Children = { homeLabel, homeButton }
			};

			this.Content = stackLayout;
		}
	}
}