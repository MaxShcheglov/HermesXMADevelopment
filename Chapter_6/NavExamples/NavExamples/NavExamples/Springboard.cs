﻿using Xamarin.Forms;

namespace NavExamples
{
	public class Springboard : ContentPage
    {
		public Springboard()
		{
			Title = "Springboard using Grid";

			Grid grid = new Grid
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				RowSpacing = 65,
				ColumnSpacing = 65,
				Padding = 60,
				RowDefinitions =
				{
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }
				},
				ColumnDefinitions =
				{
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
				}
			};

			var firstImage = new Image
			{
				Source = "icon.png",
				Aspect = Aspect.AspectFit,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			grid.Children.Add(firstImage, 0, 0);

			var secondImage = new Image
			{
				Source = "icon.png",
			};
			grid.Children.Add(secondImage, 0, 1);

			var thirdImage = new Image
			{
				Source = "icon.png",
			};
			grid.Children.Add(thirdImage, 0, 2);

			var tapFirst = new TapGestureRecognizer();
			tapFirst.Tapped += async (s, e) =>
			{
				await this.Navigation.PushAsync(new FirstPage());
			};
			firstImage.GestureRecognizers.Add(tapFirst);

			var tapSecond = new TapGestureRecognizer();
			tapSecond.Tapped += async (s, e) =>
			{
				await this.Navigation.PushAsync(new SecondPage());
			};
			secondImage.GestureRecognizers.Add(tapSecond);

			var tapThird = new TapGestureRecognizer();
			tapThird.Tapped += async (s, e) =>
			{
				await this.Navigation.PushAsync(new ThirdPage());
			};
			thirdImage.GestureRecognizers.Add(tapThird);

			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

			this.Content = grid;
		}
    }
}
