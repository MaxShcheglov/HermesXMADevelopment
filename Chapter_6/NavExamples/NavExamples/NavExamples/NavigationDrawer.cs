﻿using Xamarin.Forms;

namespace NavExamples
{
	public class NavigationDrawer : MasterDetailPage
	{
		public NavigationDrawer()
		{
			Title = "Navigation Drawer using MasterDetailPage";
			string[] myPageNames = { "Home", "Second", "Third" };

			ListView listView = new ListView
			{
				ItemsSource = myPageNames,
			};

			this.Master = new ContentPage
			{
				Title = "Options",
				Content = listView,
				Icon = "hamburger.png"
			};

			listView.ItemTapped += (sender, e) =>
			{
				ContentPage gotoPage;
				switch (e.Item.ToString())
				{
					case "Home":
						gotoPage = new HomePage();
						break;
					case "Second":
						gotoPage = new SecondPage();
						break;
					case "Third":
						gotoPage = new ThirdPage();
						break;
					default:
						gotoPage = new NavigationPage1();
						break;
				}

				Detail = new NavigationPage(gotoPage);
				((ListView)sender).SelectedItem = null;
				this.IsPresented = false;
			};

			Detail = new NavigationPage(new HomePage());
		}
	}
}