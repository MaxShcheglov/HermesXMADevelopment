﻿using System;

namespace NavExamples
{
	public class ListItemPage
    {
		public string Title { get; set; }
		public Type PageType { get; set; }
    }
}
