﻿using Xamarin.Forms;

namespace NavExamples
{
	public class Carousel : CarouselPage
    {
		public Carousel()
		{
			this.Children.Add(new FirstPage());
			this.Children.Add(new SecondPage());
			this.Children.Add(new ThirdPage());
		}
	}
}
