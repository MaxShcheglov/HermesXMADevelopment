﻿using Xamarin.Forms;

namespace NavExamples
{
	class GlobalPage2 : ContentPage
	{
		public GlobalPage2()
		{
			Title = "Second Global Page";

			var myData = Global.Instance.MyData;

			Label label = new Label
			{
				Text = "Persisted id: " + myData.ToString(),
				FontSize = 40
			};

			var stackLayout = new StackLayout
			{
				Children = { label }

			};

			this.Content = stackLayout;
		}

	}
}