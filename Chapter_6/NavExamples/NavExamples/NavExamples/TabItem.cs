﻿namespace NavExamples
{
	public class TabItem
    {
		public TabItem(string name, int number)
		{
			Name = name;
			Number = number;
		}

		public string Name { get; private set;}

		public int Number { get; private set; }
    }
}
