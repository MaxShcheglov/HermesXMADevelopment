﻿using System;

namespace NavExamplesAndroid
{
	public class ListItem
	{
		public string Title { get; set; }
		public Type PageType { get; set; }
	}

}