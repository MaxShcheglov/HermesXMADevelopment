﻿
using Android.App;
using Android.Content;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "PassStringActivity")]
	public class PassStringActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			SetContentView(Resource.Layout.PassFrom);

			Intent intent = new Intent(this, typeof(PassStringToActivity));
			intent.PutExtra("MyData", "A string of data");
			StartActivity(intent);
		}
	}
}