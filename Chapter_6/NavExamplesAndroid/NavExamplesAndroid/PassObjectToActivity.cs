﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace NavExamplesAndroid
{
	[Activity(Label = "PassObjectToActivity")]
	public class PassObjectToActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.PassTo);
			TextView text1 = FindViewById<TextView>(Resource.Id.textPassed1);
			TextView text2 = FindViewById<TextView>(Resource.Id.textPassed2);

			String json = Intent.GetStringExtra("MyData") ?? "No Data";
			DataModel data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataModel>(json);
			text1.Text = data.Id.ToString();
			text2.Text = data.Info;
		}
	}
}