﻿
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace NavExamplesAndroid
{
	public class DialogFragmentView : DialogFragment
	{
		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			base.OnCreate(savedInstanceState);

			var view = inflater.Inflate(Resource.Layout.Modal, container, false);
			view.FindViewById<Button>(Resource.Id.submitButton).Click += (sender, args) => Dismiss();
			return view;
		}
	}
}