﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace NavExamplesAndroid
{
	[Activity(Label = "IntentActivity")]
	public class IntentActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Intent);

			Button button = FindViewById<Button>(Resource.Id.MyButton);
			button.Click += delegate
			{
				Intent intent = new Intent(this, typeof(IntentToActivity));
				StartActivity(intent);
			};
		}
	}
}