﻿
using Android.App;
using Android.OS;
using Android.Widget;
using System;

namespace NavExamplesAndroid
{
	[Activity(Label = "PopupActivity")]
	public class PopupActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here

			SetContentView(Resource.Layout.PopupLayout);
			Button button = FindViewById<Button>(Resource.Id.button);

			button.Click += (s, e) =>
			{
				PopupMenu menu = new PopupMenu(this, button);
				menu.Inflate(Resource.Menu.popupmenu);

				menu.MenuItemClick += (s1, e2) =>
				{
					Console.WriteLine("{0} Selected", e2.Item.TitleFormatted);
				};
				menu.Show();
			};
		}
	}
}