﻿
using Android.App;
using Android.OS;
using Android.Widget;

namespace NavExamplesAndroid
{
	[Activity(Label = "PassStringToActivity")]
	public class PassStringToActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.PassTo);

			TextView text1 = FindViewById<TextView>(Resource.Id.textPassed1);

			Button button = FindViewById<Button>(Resource.Id.MyButton);
			string text = Intent.GetStringExtra("MyData") ?? "No Data";
			text1.Text = text;
		}
	}
}