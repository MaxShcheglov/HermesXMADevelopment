﻿
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace NavExamplesAndroid
{
	public class TabFirstFragment : Fragment
	{
		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			base.OnCreateView(inflater, container, savedInstanceState);

			var view = inflater.Inflate(Resource.Layout.TabFirst, container, false);
			var text = view.FindViewById<TextView>(Resource.Id.text);
			text.Text = "This is the first tab page";

			return view;
		}
	}
}