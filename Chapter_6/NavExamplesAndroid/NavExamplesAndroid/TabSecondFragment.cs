﻿
using Android;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace NavExamplesAndroid
{
	class TabSecondFragment : Fragment
	{
		public override View OnCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView(inflater, container, savedInstanceState);
			var view = inflater.Inflate(
						Resource.Layout.TabFirst, container, false);
			var text =
						view.FindViewById<TextView>(Resource.Id.text);
			text.Text = "This is the second tab page.";
			return view;
		}
	}
}