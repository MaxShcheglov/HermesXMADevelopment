﻿
using Android.App;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "DrilldownActivity2")]
	public class DrilldownActivity2 : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Drilldown2);

		}
	}
}