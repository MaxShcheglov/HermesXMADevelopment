﻿
using Android.App;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "FragmentsActivity")]
	public class FragmentsActivity : Activity
	{
		public FragmentsActivity()
		{
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.MainFragment);
		}
	}
}