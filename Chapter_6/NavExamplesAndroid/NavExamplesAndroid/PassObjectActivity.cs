﻿using Android.App;
using Android.Content;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "PassObjectActivity")]
	public class PassObjectActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.PassFrom);

			DataModel data = new DataModel();
			data.Id = 1;
			data.Info = "First";

			string json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
			var intent = new Intent(this, typeof(PassObjectToActivity));
			intent.PutExtra("MyData", json);

			StartActivity(intent);
		}
	}
}