﻿
using Android.App;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "DrilldownActivity1")]
	public class DrilldownActivity1 : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Drilldown1);
		}
	}
}
