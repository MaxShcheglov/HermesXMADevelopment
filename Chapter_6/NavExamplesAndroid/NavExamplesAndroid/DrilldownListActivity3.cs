﻿
using Android.App;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "DrilldownActivity3")]
	public class DrilldownActivity3 : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Drilldown3);

		}
	}
}