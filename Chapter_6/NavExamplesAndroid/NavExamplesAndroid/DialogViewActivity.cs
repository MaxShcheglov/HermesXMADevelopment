﻿
using Android.App;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "DialogViewActivity")]
	public class DialogViewActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here

			var transaction = FragmentManager.BeginTransaction();
			var dialogFragment = new DialogFragmentView();
			dialogFragment.Show(transaction, "dialog_fragment");
		}
	}
}