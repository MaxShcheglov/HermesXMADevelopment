﻿
using Android.App;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "DialogAlertActivity")]
	public class DialogAlertActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here

			var transaction = FragmentManager.BeginTransaction();
			var dialogFragment = new DialogFragmentAlert();
			dialogFragment.Show(transaction, "dialog_fragment");
		}
	}
}