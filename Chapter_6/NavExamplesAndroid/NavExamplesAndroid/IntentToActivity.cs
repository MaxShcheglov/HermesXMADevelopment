﻿using Android.App;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "IntentActivityView")]
	public class IntentToActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.IntentNew);
		}
	}
}