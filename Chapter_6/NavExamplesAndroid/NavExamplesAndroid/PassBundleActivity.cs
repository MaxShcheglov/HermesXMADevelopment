﻿using Android.App;
using Android.Content;
using Android.OS;

namespace NavExamplesAndroid
{
	[Activity(Label = "PassBundleActivity")]
	public class PassBundleActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Bundle thisBundle = new Bundle();
			thisBundle.PutString("MyData", "A string of data");
			thisBundle.PutString("MyData2", "Another string of data");
			var intent = new Intent(this, typeof(PassBundleToActivity));
			intent.PutExtras(thisBundle);
			StartActivity(intent);
		}
	}
}