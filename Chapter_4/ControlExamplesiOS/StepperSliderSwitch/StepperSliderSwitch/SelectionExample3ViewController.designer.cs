﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SelectionExample3
{
    [Register ("SelectionExample3ViewController")]
    partial class SelectionExample3ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISlider slider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel sliderLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStepper stepper { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel stepperLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel switchLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch thisSwitch { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (slider != null) {
                slider.Dispose ();
                slider = null;
            }

            if (sliderLabel != null) {
                sliderLabel.Dispose ();
                sliderLabel = null;
            }

            if (stepper != null) {
                stepper.Dispose ();
                stepper = null;
            }

            if (stepperLabel != null) {
                stepperLabel.Dispose ();
                stepperLabel = null;
            }

            if (switchLabel != null) {
                switchLabel.Dispose ();
                switchLabel = null;
            }

            if (thisSwitch != null) {
                thisSwitch.Dispose ();
                thisSwitch = null;
            }
        }
    }
}